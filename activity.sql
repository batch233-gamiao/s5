-- 1. Return the customerName of the customers who are from the Philippines
SELECT customerName FROM customers WHERE country = 'Philippines';

-- 2. Return the contactLastName and contactFirstName of customers with name "La Rochelle Gifts"
SELECT contactLastName, contactFirstName FROM customers WHERE customerName = 'La Rochelle Gifts';

-- 3. Return the product name and MSRP of the product named "The Titanic"
SELECT productName, MSRP from products WHERE productName = 'The Titanic';

-- 4. Return the first and last name of the employee whose email is "jfirreli@classicmodelcars.com"
SELECT lastName, firstName FROM employees WHERE email = 'jfirrelli@classicmodelcars.com';

-- 5. Return the names of customers who have no registered state
SELECT customerName FROM customers WHERE state IS NULL;

-- 6. Return the first name, last name, email of the employee whose last name is Patterson and first name is Steve
SELECT lastName, firstName, email FROM employees WHERE lastName = 'Patterson' AND firstName = 'Steve';

-- 7. Return customer name, country, and credit limit of customers whose countries are NOT USA and whose credit limits are greater than 3000
SELECT customerName, country, creditLimit FROM customers WHERE country != 'USA' AND creditLimit > 3000;

-- 8. Return the customer numbers of orders whose comments contain the string 'DHL'
SELECT customerNumber FROM orders WHERE comments LIKE '%DHL%';

-- 9. Return the product lines whose text description mentions the phrase 'state of the art'
SELECT productLine FROM productlines WHERE textDescription LIKE '%state of the art%';

-- 10. Return the countries of customers without duplication
SELECT DISTINCT country FROM customers;

-- 11. Return the statuses of orders without duplication
SELECT DISTINCT status FROM orders;

-- 12. Return the customer names and countries of customers whose country is USA, France, or Canada
SELECT customerName, country FROM customers WHERE country IN ('USA','France','Canada');

-- 13. Return the first name, last name, and office's city of employees whose offices are in Tokyo
SELECT employees.firstName, employees.lastName, offices.city FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = 'Tokyo';

-- 14. Return the customer names of customers who were served by the employee named "Leslie Thompson"
SELECT customerName FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
WHERE employees.firstName = 'Leslie' AND employees.lastName = 'Thompson';

-- 15. Return the product names and customer name of products ordered by "Baane Mini Imports"
SELECT products.productName, customers.customerName FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = 'Baane Mini Imports';

-- 16. Return the employees' first names, employees' last names, customers' names, and offices' countries of transactions whose customers and offices are in the same country
SELECT employees.firstName, employees.lastName, customers.customerName, offices.country FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber
JOIN offices ON employees.officeCode = offices.officeCode
WHERE customers.country = offices.country;

-- 17. Return the product name and quantity in stock of products that belong to the product line "planes" with stock quantities less than 1000
SELECT productName, quantityInStock FROM products
WHERE productLine = 'planes' AND quantityInStock < 1000;

-- 18. Return the customer's name with a phone number containing "+81".
SELECT customerName, phone FROM customers WHERE phone LIKE '%+81%';

-- ----------------------------------------------------------------------------------------------------
/*Stretch Goal*/
-- 1. -- Return the product name of the orders where customer name is Baane Mini Imports
SELECT products.productName FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = 'Baane Mini Imports';

-- 2. --  Return the last name and first name of employees that reports to Anthony Bow
SELECT lastName, firstName FROM employees
WHERE reportsTo = 1143;

-- 3. -- Return the product name of the product with the maximum MSRP
SELECT productName FROM products
WHERE MSRP=(select max(MSRP) from products);

-- 4. -- Return the number of products group by productline
SELECT count(productName), productlines.productLine FROM products
JOIN productlines ON products.productLine = productlines.productLine
GROUP BY productlines.productLine;

-- 5. -- Return the number of products where the status is cancelled
SELECT count(orders.status) AS 'COUNT(orderNumber)' FROM orders
WHERE orders.status = 'Cancelled';